package ontology;

import com.hp.hpl.jena.rdf.model.Model;
//import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.OWL;
import com.hp.hpl.jena.vocabulary.RDFS;

/**
 * @author A. Tzani
 */
public class OntologyInitialization {
	
	/**
     * Add the necessary prefixes to the model we are currently 
     * working with.
     * 
     * @param Model the model we are currently working with
     */
	public void setPrefixes(Model model) {
		model.setNsPrefix("elod", Ontology.eLodPrefix);
		model.setNsPrefix("skos", Ontology.skosPrefix);
    	model.setNsPrefix("gr", Ontology.goodRelationsPrefix);
    	model.setNsPrefix("rov", Ontology.regOrgPrefix);
    	model.setNsPrefix("org", Ontology.orgPrefix);
    	model.setNsPrefix("foaf", Ontology.foafPrefix);
    	model.setNsPrefix("elodGeo", Ontology.elodGeoPrefix);
    	}
	
	/**
	 * Create the basic hierarchies of the OWL classes and their labels 
	 * to the model we are currently working with.
	 * 
	 * @param Model the model we are currently working with
	 */
	public void createHierarchies(Model model) {
		
		//Agent
		model.add(Ontology.agentResource, RDFS.subClassOf, OWL.Thing);
		model.add(Ontology.agentResource, RDFS.label, model.createLiteral("Agent", "en"));
		model.add(Ontology.agentResource, RDFS.label, model.createLiteral("Πράκτορας", "el"));
		//Agent - Organization (FOAF)
		model.add(Ontology.organizationResource, RDFS.subClassOf, Ontology.agentResource);
		model.add(Ontology.organizationResource, RDFS.label, model.createLiteral("Organization", "en"));
		model.add(Ontology.organizationResource, RDFS.label, model.createLiteral("Οργανισμός", "el"));
		
		//Agent - BusinessEntity
		model.add(Ontology.businessEntityResource, RDFS.subClassOf, Ontology.agentResource);
		model.add(Ontology.businessEntityResource, RDFS.label, model.createLiteral("Business Entity", "en"));
		model.add(Ontology.businessEntityResource, RDFS.label, model.createLiteral("Επιχειρηματική Οντότητα", "el"));
		
		//Agent - Organization (org)
		model.add(Ontology.orgOrganizationResource, RDFS.subClassOf, Ontology.agentResource);
		model.add(Ontology.orgOrganizationResource, RDFS.label, model.createLiteral("Organization", "en"));
		model.add(Ontology.orgOrganizationResource, RDFS.label, model.createLiteral("Οργανισμός", "el"));
		
		//Agent - Registered Organization
		model.add(Ontology.registeredOrganizationResource, RDFS.subClassOf, Ontology.agentResource);
		model.add(Ontology.registeredOrganizationResource, RDFS.label, model.createLiteral("Registered Organization", "en"));
		model.add(Ontology.registeredOrganizationResource, RDFS.label, model.createLiteral("Καταχωρημένος Οργανισμός", "el"));
				
		//Rank															
		model.add(Ontology.rankResource, RDFS.subClassOf, OWL.Thing);		
		model.add(Ontology.rankResource, RDFS.label, model.createLiteral("Rank", "en"));
		model.add(Ontology.rankResource, RDFS.label, model.createLiteral("Κατάταξη", "el"));																		
		
		//Fundamentals															
		model.add(Ontology.fundamentalResource, RDFS.subClassOf, OWL.Thing);		
		model.add(Ontology.fundamentalResource, RDFS.label, model.createLiteral("Fundamental", "en"));
		model.add(Ontology.fundamentalResource, RDFS.label, model.createLiteral("Βασικές Αρχές", "el"));
		
		//Currency															
		model.add(Ontology.currencyResource, RDFS.subClassOf, OWL.Thing);		
		model.add(Ontology.currencyResource, RDFS.label, model.createLiteral("Currency", "en"));
		model.add(Ontology.currencyResource, RDFS.label, model.createLiteral("Nόμισμα", "el"));
		
		//Country															
		model.add(Ontology.countryResource, RDFS.subClassOf, OWL.Thing);		
		model.add(Ontology.countryResource, RDFS.label, model.createLiteral("Country", "en"));
		model.add(Ontology.countryResource, RDFS.label, model.createLiteral("Χώρα", "el"));
		
		//Relates															
		//model.add(Ontology.relatesResource, RDFS.subClassOf, OWL.Thing);		
		//model.add(Ontology.relatesResource, RDFS.label, model.createLiteral("Relates", "en"));
		//model.add(Ontology.relatesResource, RDFS.label, model.createLiteral("Αντιστοιχίες", "el"));
				
		//Concepts
		model.add(Ontology.conceptsResource, RDFS.subClassOf, OWL.Thing);
		
		//Concepts - Forbes Sector
		model.add(Ontology.forbesSectorResource, RDFS.subClassOf, Ontology.conceptsResource);
		model.add(Ontology.forbesSectorResource, RDFS.label, model.createLiteral("Forbes Sector", "en"));
		model.add(Ontology.forbesSectorResource, RDFS.label, model.createLiteral("Τομέας Φορμπς", "el"));
		
		//Concepts - Forbes Industry
		model.add(Ontology.forbesIndustryResource, RDFS.subClassOf, Ontology.conceptsResource);
		model.add(Ontology.forbesIndustryResource, RDFS.label, model.createLiteral("Forbes Industry", "en"));
		model.add(Ontology.forbesIndustryResource, RDFS.label, model.createLiteral("Βιομηχανία Φορμπς", "el"));
		
	}
	
}

