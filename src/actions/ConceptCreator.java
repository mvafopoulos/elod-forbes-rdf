package actions;

import java.util.Arrays;
import java.util.List;

import ontology.Ontology;
import utils.HelperMethods;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * @author A. Tzanis
 */
public class ConceptCreator {
	
	private HelperMethods hm = new HelperMethods();
	
	/**
     * Add to the model the forbesSector.
     * 
     * @param Model the model we are currently working with
     */
	public void addSectorToModel(Model model) {
		
		List<String> forbesSectorList = Arrays.asList("Financials", "Energy", "Industrials", "Consumer Discretionary", "Information Tecnology", "Telecommunication Services",
												"Consumer Staples", "Health Care", "Materials" , "Utilities");
		
		for (String forbesSector : forbesSectorList) {
			String[] forbesSectorDtls = hm.findForbesSectorIndividual(forbesSector);
			/** sectorResource **/
			Resource forbesSectorResource = model.createResource(forbesSectorDtls[0], Ontology.forbesSectorResource);
			model.createResource(forbesSectorDtls[0], Ontology.conceptsResource);
			/** configure prefLabel **/
			forbesSectorResource.addProperty(Ontology.prefLabel, forbesSectorDtls[2], "el");
			forbesSectorResource.addProperty(Ontology.prefLabel, forbesSectorDtls[1], "en");
        }
		
	}
	
	/**
     * Add to the model the Forbes Industries Type .
     * 
     * @param Model the model we are currently working with
     */
	public void addForbesIndustryToModel(Model model) {
		
		List<String> forbesIndustryList = Arrays.asList("Major Banks", "Regional Banks", "Investment Services", "Oil & Gas Operations", "Conglomerates", "Auto & Truck Manufacturers", 
													  "Computer Hardware", "Discount Stores", "Semiconductors", "Telecommunications Services", "Diversified Insurance", "Software & Programming",
													  "Computer Services", "Food Processing", "Household-Personal Care", "Medical Equipment & Supplies", "Diversified Metals & Mining", "Pharmaceuticals",
													  "Electric Utilities", "Broadcasting & Cable", "Beverages", "Life & Health Insurance", "Diversified Chemicals", "Communications & Equipment", 
													  "Aerospace & Defense", "Managed Health Care", "Drug Retail", "Consumer Financial Servises", "Oil Services & Equipment", "Heavy Equipment", "Trading Companies",
													  "Home Improvement Retail", "Electronics", "Other Transportation", "Construction Services", "Tobacco", "Healthcare Services", "Air Courier", "Airline",
													  "Natural Gas Utilities", "Biotechs", "Property & Casualty Insurance", "Rairoads", "Restaurants", "Apparel-Accessories", "Electrical Equipment",
													  "Auto & Truck Parts", "Iron & Steel", "Food Retail", "Computer Storage Devices", "Business Products & Supplies", "Consumer Electronics", "Real Estate",
													  "Internet & Catalog Retail", "Construction Materials", "Specialized Chemicals", "Other Industrial Equipment", "Apparel-Footware Retail", "Advertising",
													  "Casinos & Gaming", "Paper & Paper Products", "Specialty Stores", "Hotels & Motels", "Precision Healthcare Equipment", "Business & Personal Services",
													  "Departmanent Stores", "Insurance Brokers", "Houshold Appliances", "Furniture & Fixtures", "Diversified Utilities", "Aluminium", "Printing & Publishing",
													  "Computer & Electronics Retail", "Security Systems", "Environmental & Waste", "Rental & Leasing", "Containers & Packaging", "Recreational Products",
													  "Trucking", "Thrifts & Mortgage Finance");
		
		for (String forbesIndustry : forbesIndustryList) {
			String[] forbesIndustryDtls = hm.findForbesIndustryIndividual(forbesIndustry);
			/** forbesIndustryResource **/
			Resource forbesIndustryResource = model.createResource(forbesIndustryDtls[0], Ontology.forbesIndustryResource);
			model.createResource(forbesIndustryDtls[0], Ontology.conceptsResource);
			/** configure prefLabel **/
			forbesIndustryResource.addProperty(Ontology.prefLabel, forbesIndustryDtls[2], "el");
			forbesIndustryResource.addProperty(Ontology.prefLabel, forbesIndustryDtls[1], "en");
        }
		
	}
	
}