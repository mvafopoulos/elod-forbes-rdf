package objects;

import java.util.ArrayList;

/**
 * @author A. Tzani
 */

public class ExcelObject {
	public String CompanyName = null; 
	public String CompanyIsin = null; 
	public String Sector = null;
	public String Industry = null;
	public String Continent = null;
	public String Country = null;
	public double MarketValue = 0;
	public double Sales = 0;
	public static double Profits = 0;
	public double Assets = 0;
	public int Rank = 0;
	public String WebPage = null;
	public ArrayList<String> MatchedUris = null;

	@SuppressWarnings("static-access")
	public ExcelObject(String companyName,String companyIsin,String sector, String industry, String continent, String country, double marketValue, 
			double sales, double profits, double assets, int rank,String webPage, ArrayList<String> matchedUris) {
		
		this.CompanyName = companyName;
		this.CompanyIsin = companyIsin;
		this.Sector = sector;
		this.Industry = industry;
		this.Continent = continent;
		this.Country = country;
		this.MarketValue = marketValue;
		this.Sales = sales;
		this.Profits = profits;
		this.Assets = assets;
		this.Rank = rank;
		this.WebPage = webPage;
		///////////////////////////////////////////// eginan allages ///////////////////////////////
		this.MatchedUris = matchedUris;
		
	}

	public String getCompanyName() {
		return CompanyName;
	}

	public void setCompanyName(String companyName) {
		this.CompanyName = companyName;
	}

	public String getCompanyIsin() {
		return CompanyIsin;
	}

	public void setCompanyIsin(String companyIsin) {
		this.CompanyIsin = companyIsin;
	}

	public String getSector() {
		return Sector;
	}

	public void setSector(String sector) {
		this.Sector = sector;
	}

	public String getContinent() {
		return Continent;
	}

	public void setContinent(String industry) {
		this.Continent = industry;
	}
	public String getIndustry() {
		return Industry;
	}

	public void setIndustry(String industry) {
		this.Industry = industry;
	}

	
	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		this.Country = country;
	}

	public double getMarketValue() {
		return MarketValue;
	}

	public void setMarketValue(double marketValue) {
		this.MarketValue = marketValue;
	}

	public double getSales() {
		return Sales;
	}

	public void setSales(double sales) {
		this.Sales = sales;
	}

	public double getProfits() {
		return Profits;
	}

	@SuppressWarnings("static-access")
	public void setProfits(double profits) {
		this.Profits = profits;
	}

	public double getAssets() {
		return Assets;
	}

	public void setAssets(double assets) {
		this.Assets = assets;
	}

	public int getRank() {
		return Rank;
	}

	public void setRank(int rank) {
		this.Rank = rank;
	}

	public String getWebPage() {
		return WebPage;
	}

	public void setWebPage(String webPage) {
		this.WebPage = webPage;
	}

	public ArrayList<String> getMatchedUri() {
		return MatchedUris;
	}

	public void setMatchedUri(ArrayList<String> matchedUris) {
		this.MatchedUris = matchedUris;
	}
}